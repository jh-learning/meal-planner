# Server

**TODO: Add description**

## Installation

Install dependencies with `mix deps.get`

To compile, run `mix compile`

## Tests

Tests are run using `mix test`


## Running

TODO


## Formatting

Code can be auto formatted by running `mix format`


## Documentation

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at <https://hexdocs.pm/server>.

