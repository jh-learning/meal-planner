-- Unit of measurement
CREATE TABLE Unit (
    UnitID INTEGER NOT NULL PRIMARY KEY,
    Name TEXT
);

-- A store like a supermarket or grocery
CREATE TABLE Store (
    StoreID INTEGER NOT NULL PRIMARY KEY,
    Name TEXT,
    Note TEXT
);

-- A generic ingredient item, e.g. milk
CREATE TABLE Ingredient (
    IngredientID INTEGER NOT NULL PRIMARY KEY,
    Name TEXT
);

-- An identifier for filtering, e.g. by country or type
CREATE TABLE Tag (
    TagID INTEGER NOT NULL PRIMARY KEY,
    Name TEXT
);

-- A recipe in question
CREATE TABLE Recipe (
    RecipeID INTEGER PRIMARY KEY,
    Title TEXT,
    Image BLOB,
    Desc TEXT,
    Note TEXT
);


-- Specific Store data for said Ingredient, e.g. New World Pam's milk 2L
CREATE TABLE IngredientStore (
    IngredientStoreID INTEGER NOT NULL PRIMARY KEY,
    IngredientID INTEGER NOT NULL,
    StoreID INTEGER NOT NULL,
    UnitID INTEGER NOT NULL,
    Name TEXT,
    Price REAL,
    Quantity REAL,
    FOREIGN KEY (IngredientID) REFERENCES Ingredient(IngredientID),
    FOREIGN KEY (StoreID) REFERENCES Store(StoreID),
    FOREIGN KEY (UnitID) REFERENCES Unit(UnitID),
    UNIQUE (IngredientID, StoreID)
);

-- A list of ingredients for a recipe
CREATE TABLE RecipeIngredient (
    RecipeIngredientID INTEGER NOT NULL PRIMARY KEY,
    RecipeID INTEGER NOT NULL,
    IngredientID INTEGER NOT NULL,
    Quantity TEXT,
    Prepped TEXT,
    FOREIGN KEY (RecipeID) REFERENCES Recipe(RecipeID),
    FOREIGN KEY (IngredientID) REFERENCES Ingredient(IngredientID),
    UNIQUE (RecipeID, IngredientID)
);

-- A list of steps for a recipe
CREATE TABLE RecipeStep (
    StepID INTEGER NOT NULL PRIMARY KEY,
    RecipeID INTEGER NOT NULL,
    StepNum INTEGER NOT NULL,
    StepText TEXT,
    FOREIGN KEY (RecipeID) REFERENCES Recipe(RecipeID),
    UNIQUE (RecipeID, StepNum)
);

-- A list of tags for a recipe
CREATE TABLE RecipeTag (
    RecipeTagID INTEGER NOT NULL PRIMARY KEY,
    RecipeID INTEGER NOT NULL,
    TagID INTEGER NOT NULL,
    FOREIGN KEY (RecipeID) REFERENCES Recipe(RecipeID),
    FOREIGN KEY (TagID) REFERENCES Tag(TagID),
    UNIQUE (RecipeID, TagID)
);